'use strict'

var gulp = require('gulp'),
    sass = require('sass'),
    browserSync = require('browser-sync');

gulp.task('sass', function(){
    gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function(){
    gulp.watch('./css/*.scss',['sass']);
});

gulp.task('browser-sync', function(){
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', ['browser-sync'], function(){
    gulp.setMaxListeners('sass: watch');
});