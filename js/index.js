$(function(){
    $("[data-toggle='popover']").popover();
    $("[data-toggle='tooltip']").tooltip();
    $(".carousel").carousel({
        interval: 4000
    });
    $('#contacto').on('show.bs.modal', function(e){
        console.log('modal.show');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('modal.shown');
    });      
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('modal.hide');
    });    
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('modal.hidden');
        $('#contactoBtn').prop('disabled', false);
    });    
});